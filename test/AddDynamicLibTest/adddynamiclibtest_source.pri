SOURCES += \
        $${PWD}/adddynamiclibtest.cpp

HEADERS += \
        $${PWD}/adddynamiclibtest.h \
        $${PWD}/adddynamiclibtest_global.h

include($${PWD}/adddynamiclibtest_3rdparty.pri)

add_object_class(TestObjClass)
SOURCES += testobjclass.cpp
HEADERS += testobjclass.h


add_thread_class(TestThreadClass)
SOURCES += testthreadclass.cpp
HEADERS += testthreadclass.h

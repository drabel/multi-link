#ifndef TESTTHREADCLASS_H
#define TESTTHREADCLASS_H

#include <QThread>

class TestThreadClass : public QThread
{
    Q_OBJECT

public:
    explicit TestThreadClass ( QObject* parent = 0 )
        : QThread ( parent ) {}
    virtual ~TestThreadClass() {}

protected:

private:


    // QThread interface
protected:
    virtual void run() override {
    }
};

#endif // TESTTHREADCLASS_H

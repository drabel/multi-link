#ifndef TESTOBJCLASS_H
#define TESTOBJCLASS_H

#include <QObject>

class TestObjClass : public QObject
{
    Q_OBJECT

public:
    explicit TestObjClass ( QObject* parent = 0 )
        : QObject ( parent ) {}
    virtual ~TestObjClass() {}

protected:

private:

};

#endif // TESTOBJCLASS_H

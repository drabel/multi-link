#---------------------------------------------------------------------------------
#add_files.pri
#2020-04-30 11:01:24
#Qt工程常用的类，方便添加类的函数。
#
#please don't modify this pri
#---------------------------------------------------------------------------------

#add_file()
#add_class()
#add_object_class()
#add_widget_class()
#add_thread_class()

#add_class_to_qmake()
#add_object_class_to_qmake()
#add_widget_class_to_qmake()

################################################################################
#内部用函数
#获取命令
################################################################################
ADD_FILES_PRI_PWD = $${PWD}

################################################################################
#外部用函数
#获取命令
################################################################################

#不存在就创建，存在就返回。
#create file
defineTest(add_file){
    file_name = $$1
    exists($${file_name}):return(0)
    empty_file($${file_name})
    return(1)
}

#create class file
defineTest(add_class){
    isEmpty(1): error("add_class(classname, workpath) requires at last one argument")
    classname=$$1
    isEmpty(1):return(0)

    workpath=$${PWD}
    !isEmpty(2):workpath=$$2

    filename = $$lower($${classname})
    bigname = $$upper($${classname})
    FILE_HPP = $${workpath}/$${filename}.h
    FILE_CPP = $${workpath}/$${filename}.cpp
    equals(QMAKE_HOST.os, Windows) {
        FILE_HPP~=s,/,\\,g
        FILE_CPP~=s,/,\\,g
    }

    exists($${FILE_HPP}):return(0)
    exists($${FILE_CPP}):return(0)

    equals(QMAKE_HOST.os, Windows) {
        ret = $$system("$${ADD_FILES_PRI_PWD}/win_create_class_file.bat $${classname} $${workpath}")
    } else {
        ret = $$system("chmod +x $${ADD_FILES_PRI_PWD}/unix_create_class_file.sh")
        ret = $$system("$${ADD_FILES_PRI_PWD}/unix_create_class_file.sh $${classname} $${workpath}")
    }

    message($$TARGET create class $${classname})
    message($$TARGET create $${FILE_HPP})
    message($$TARGET create $${FILE_CPP})

    return (1)
}

#create class file based on QObject
defineTest(add_object_class){
    isEmpty(1): error("add_object_class(classname, workpath) requires at last one argument")
    classname=$$1
    isEmpty(1):return(0)

    workpath=$${PWD}
    !isEmpty(2):workpath=$$2

    filename = $$lower($${classname})
    bigname = $$upper($${classname})
    FILE_HPP = $${workpath}/$${filename}.h
    FILE_CPP = $${workpath}/$${filename}.cpp

    equals(QMAKE_HOST.os, Windows) {
        FILE_HPP~=s,/,\\,g
        FILE_CPP~=s,/,\\,g
    }

    exists($${FILE_HPP}):return(0)
    exists($${FILE_CPP}):return(0)

    equals(QMAKE_HOST.os, Windows) {
        ret = $$system("$${ADD_FILES_PRI_PWD}/win_create_class_file_object.bat $${classname} $${workpath}")
    } else {
        ret = $$system("chmod +x $${ADD_FILES_PRI_PWD}/unix_create_class_file_object.sh")
        ret = $$system("$${ADD_FILES_PRI_PWD}/unix_create_class_file_object.sh $${classname} $${workpath}")
    }

    message($$TARGET create class $${classname})
    message($$TARGET create $${FILE_HPP})
    message($$TARGET create $${FILE_CPP})

    return (1)
}

#create class file based on QWidget
defineTest(add_widget_class){
    isEmpty(1): error("add_widget_class(classname, workpath) requires at last one argument")
    classname=$$1
    isEmpty(1):return(0)

    workpath=$${PWD}
    !isEmpty(2):workpath=$$2

    filename = $$lower($${classname})
    bigname = $$upper($${classname})
    FILE_HPP = $${workpath}/$${filename}.h
    FILE_CPP = $${workpath}/$${filename}.cpp
    equals(QMAKE_HOST.os, Windows) {
        FILE_HPP~=s,/,\\,g
        FILE_CPP~=s,/,\\,g
    }

    exists($${FILE_HPP}):return(0)
    exists($${FILE_CPP}):return(0)

    equals(QMAKE_HOST.os, Windows) {
        ret = $$system("$${ADD_FILES_PRI_PWD}/win_create_class_file_widget.bat $${classname} $${workpath}")
    } else {
        ret = $$system("chmod +x $${ADD_FILES_PRI_PWD}/unix_create_class_file_widget.sh")
        ret = $$system("$${ADD_FILES_PRI_PWD}/unix_create_class_file_widget.sh $${classname} $${workpath}")
    }

    message($$TARGET create class $${classname})
    message($$TARGET create $${FILE_HPP})
    message($$TARGET create $${FILE_CPP})

    return (1)
}

#create class file based on QThread
defineTest(add_thread_class){
    isEmpty(1): error("add_thread_class(classname, workpath) requires at last one argument")
    classname=$$1
    isEmpty(1):return(0)

    workpath=$${PWD}
    !isEmpty(2):workpath=$$2

    filename = $$lower($${classname})
    bigname = $$upper($${classname})
    FILE_HPP = $${workpath}/$${filename}.h
    FILE_CPP = $${workpath}/$${filename}.cpp

    equals(QMAKE_HOST.os, Windows) {
        FILE_HPP~=s,/,\\,g
        FILE_CPP~=s,/,\\,g
    }

    exists($${FILE_HPP}):return(0)
    exists($${FILE_CPP}):return(0)

    equals(QMAKE_HOST.os, Windows) {
        ret = $$system("$${ADD_FILES_PRI_PWD}/win_create_class_file_thread.bat $${classname} $${workpath}")
    } else {
        ret = $$system("chmod +x $${ADD_FILES_PRI_PWD}/unix_create_class_file_thread.sh")
        ret = $$system("$${ADD_FILES_PRI_PWD}/unix_create_class_file_thread.sh $${classname} $${workpath}")
    }

    message($$TARGET create class $${classname})
    message($$TARGET create $${FILE_HPP})
    message($$TARGET create $${FILE_CPP})

    return (1)
}
